## FanficPlus ##

FanficPlus is a Chrome add-on that enables some extra functionality for 
[Fanfiction.net](http://www.fanfiction.net/). As of this moment, it:

- allows the blacklisting of individual fics such that they never show up on a
  story listing page;
- allows previously ignored fics to be un-ignored;
- allows whole-page blacklisting (i.e. if everything is dislikable, then hide it
  all!)
- allows clearing all ignores
- allows showing all hidden ignores

Ignored fanfics will not show up in the listing at all. You'll know they've been
skipped, because the fics will still be rendered in order, but you won't see
them.

Future features include:

- cross-computer sync of lists;
- regular expression summary filter (i.e. filter out all stories containing 
  'slash', but not 'no slash')
- author ignore

FanficPlus was very much inspired by Serdel's 
[Fanfiction.net Filter for Firefox](http://serdel.net/ffnetfilter/), and if
you're stuck using Firefox, I heartily recommend it.

## How to Install ##

Just download the .CRX file [from Github](http://github.com/robyurkowski/fanficplus/raw/master/releases/fanficplus-latest.crx). 
Chrome will tell you that it does not trust extensions from this site -- but the
site is just Github, a popular code-hosting site. Find the .CRX file that you
downloaded and drag it into Chrome.

Otherwise, clone the repo, and open the .CRX with Chrome, or:

1. Open the Extension window;
2. Expand the 'Developer Mode' tab;
3. Click 'Load unpacked extension'; and
4. Choose the fanficplus folder.

That's it! Note that in the last case, you won't get automatic updates.
