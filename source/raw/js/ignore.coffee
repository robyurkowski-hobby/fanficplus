$ = jQuery

icons =
  del: chrome.extension.getURL    "source/compiled/images/delete.png"
  add: chrome.extension.getURL    "source/compiled/images/add.png"
  undo: chrome.extension.getURL   "source/compiled/images/undo.png"
  show: chrome.extension.getURL   "source/compiled/images/show.png"


# TODO: Split this out into model, serializer, collection, etc..
class IgnoreManager
  load: =>
    if localStorage.ignores then localStorage.ignores.split("|") else []

  dump: (data) =>
    localStorage.ignores = data.join("|")

  count: =>
    @load().length

  length: =>
    @count()

  addIgnore: (id) =>
    ignores = @load()
    if @hasIgnore id
      false
    else
      ignores.push id
      @dump ignores
      true

  removeIgnore: (id) =>
    ignores = @load()
    if @hasIgnore id
      ignores.splice ignores.indexOf(id), 1
      @dump ignores
      true
    else
      flase
  
  hasIgnore: (id) =>
    @load().indexOf(id) isnt -1

  truncateIgnores: =>
    @dump []

  last: =>
    @load().pop()

class StoryListView
  $entryEl:         -> $("div.z-list")
  $controlsEl:      -> $("#content_wrapper_inner")
  $ignoreEl:        -> $('.fanficplus-ignore')
  $clearAllEl:      -> $('.fanficplus-clear_all')
  $ignoreAllEl:     -> $('.fanficplus-ignore_all')
  $undoLastEl:      -> $(".fanficplus-undo_last")
  $revealHiddenEl:  -> $('.fanficplus-reveal_hidden')

  constructor: ->
    @manager = new IgnoreManager()
    # console.log "Loading ignore manager. Currently ignoring #{@manager.count()} stories."

  revealHidden: =>
    @$entryEl().each (i, e) =>
      $(e).addClass('ignored') if @manager.hasIgnore e.id
      $(e).show()

  undoLastIgnore: =>
    lastEntryId = @manager.last()

    if lastEntryId?
      @manager.removeIgnore lastEntryId
      $("##{lastEntryId}").removeClass("ignored")
        .show()
        .find(".fanficplus-ignore")
        .html "<img class='fanficplus-del' src='#{icons.del}' alt='Ignore' />"
      true

    else
      false

  clearIgnores: =>
    @manager.truncateIgnores()
    @$entryEl().each (i, e) =>
      target = $(e).find(".fanficplus-ignore")
      target.html("<img class='fanficplus-del' src='#{icons.del}' alt='Ignore' />")
      target.parent().removeClass("ignored").show()
    true

  ignoreAll: =>
    @$entryEl().each (i, e) =>
      @manager.addIgnore e.id
      target = $(e).find(".fanficplus-ignore")
      target.html "<img class='fanficplus-add' src='#{icons.add}' alt='Unignore' />"
      target.parent().hide()

  insertHeaderLinks: =>
    @$controlsEl().prepend "<div id='fanficplus-controls' class='clearfix'></div>"
    target = $("#fanficplus-controls")

    controls =
      clear_all:
        klass: 'add'
        src: icons.add
        text: 'Clear All Ignores'
      ignore_all:
        klass: 'del'
        src: icons.del
        text: 'Ignore all'
      undo_last:
        klass: 'undo'
        src: icons.undo
        text: 'Undo last ignore'
      reveal_hidden:
        klass: 'show'
        src: icons.show
        text: 'Reveal Hidden'

    controls = _.map controls, (v, k) =>
      "<a href='#' class='fanficplus-#{k}'>
       <img class='fanficplus-#{v.klass}' src='#{v.src}' alt='#{v.text}' />
       #{v.text}
       </a>"

    target.append controls.join "&nbsp;|&nbsp;"
    @addHeaderEvents()

  addHeaderEvents: =>
    @$ignoreEl().click (e) =>
      id = $(e.currentTarget).attr('data-storyid')
      if @manager.hasIgnore id
        @manager.removeIgnore id
        $(e.currentTarget).html "<img class='fanficplus-del' src='#{icons.del}' alt='Ignore' />"
        $(e.currentTarget).parent().toggleClass('ignored').show()
      else
        @manager.addIgnore id
        $(e.currentTarget).html "<img class='fanficplus-add' src='#{icons.add}' alt='Unignore' />"
        $(e.currentTarget).parent().hide()
      false

    @$clearAllEl().click =>
      @clearIgnores() if confirm "Are you sure you want to clear all ignores? This is irreversible!"
      false

    @$ignoreAllEl().click =>
      @ignoreAll() if confirm "Do you really want to ignore all fanfic on this page? This is reversible, but it's a pretty tedious process."
      false

    @$undoLastEl().click =>
      @undoLastIgnore()
      false

    @$revealHiddenEl().click =>
      @revealHidden()
      false

$ ->
  view = new StoryListView()

  view.$entryEl().find('a:first-child').each (i, e) ->
    id = e.href.split("/")[4]
    entry = $(e).parent()
    entry.attr "id", id
    if view.manager.hasIgnore(id)
      entry.prepend "<a href='#' class='fanficplus-ignore' data-storyid='#{id}'><img class='fanficplus-add' src='#{icons.add}' alt='Unignore' /></a>&nbsp;"
      entry.hide()
    else
      entry.prepend "<a href='#' class='fanficplus-ignore' data-storyid='#{id}'><img class='fanficplus-del' src='#{icons.del}' alt='Ignore' /></a>&nbsp;"

  view.insertHeaderLinks()
