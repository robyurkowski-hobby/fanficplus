(function() {
  var $, IgnoreManager, StoryListView, icons,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  $ = jQuery;

  icons = {
    del: chrome.extension.getURL("source/compiled/images/delete.png"),
    add: chrome.extension.getURL("source/compiled/images/add.png"),
    undo: chrome.extension.getURL("source/compiled/images/undo.png"),
    show: chrome.extension.getURL("source/compiled/images/show.png")
  };

  IgnoreManager = (function() {

    function IgnoreManager() {
      this.last = __bind(this.last, this);

      this.truncateIgnores = __bind(this.truncateIgnores, this);

      this.hasIgnore = __bind(this.hasIgnore, this);

      this.removeIgnore = __bind(this.removeIgnore, this);

      this.addIgnore = __bind(this.addIgnore, this);

      this.length = __bind(this.length, this);

      this.count = __bind(this.count, this);

      this.dump = __bind(this.dump, this);

      this.load = __bind(this.load, this);

    }

    IgnoreManager.prototype.load = function() {
      if (localStorage.ignores) {
        return localStorage.ignores.split("|");
      } else {
        return [];
      }
    };

    IgnoreManager.prototype.dump = function(data) {
      return localStorage.ignores = data.join("|");
    };

    IgnoreManager.prototype.count = function() {
      return this.load().length;
    };

    IgnoreManager.prototype.length = function() {
      return this.count();
    };

    IgnoreManager.prototype.addIgnore = function(id) {
      var ignores;
      ignores = this.load();
      if (this.hasIgnore(id)) {
        return false;
      } else {
        ignores.push(id);
        this.dump(ignores);
        return true;
      }
    };

    IgnoreManager.prototype.removeIgnore = function(id) {
      var ignores;
      ignores = this.load();
      if (this.hasIgnore(id)) {
        ignores.splice(ignores.indexOf(id), 1);
        this.dump(ignores);
        return true;
      } else {
        return flase;
      }
    };

    IgnoreManager.prototype.hasIgnore = function(id) {
      return this.load().indexOf(id) !== -1;
    };

    IgnoreManager.prototype.truncateIgnores = function() {
      return this.dump([]);
    };

    IgnoreManager.prototype.last = function() {
      return this.load().pop();
    };

    return IgnoreManager;

  })();

  StoryListView = (function() {

    StoryListView.prototype.$entryEl = function() {
      return $("div.z-list");
    };

    StoryListView.prototype.$controlsEl = function() {
      return $("#content_wrapper_inner");
    };

    StoryListView.prototype.$ignoreEl = function() {
      return $('.fanficplus-ignore');
    };

    StoryListView.prototype.$clearAllEl = function() {
      return $('.fanficplus-clear_all');
    };

    StoryListView.prototype.$ignoreAllEl = function() {
      return $('.fanficplus-ignore_all');
    };

    StoryListView.prototype.$undoLastEl = function() {
      return $(".fanficplus-undo_last");
    };

    StoryListView.prototype.$revealHiddenEl = function() {
      return $('.fanficplus-reveal_hidden');
    };

    function StoryListView() {
      this.addHeaderEvents = __bind(this.addHeaderEvents, this);

      this.insertHeaderLinks = __bind(this.insertHeaderLinks, this);

      this.ignoreAll = __bind(this.ignoreAll, this);

      this.clearIgnores = __bind(this.clearIgnores, this);

      this.undoLastIgnore = __bind(this.undoLastIgnore, this);

      this.revealHidden = __bind(this.revealHidden, this);
      this.manager = new IgnoreManager();
    }

    StoryListView.prototype.revealHidden = function() {
      var _this = this;
      return this.$entryEl().each(function(i, e) {
        if (_this.manager.hasIgnore(e.id)) {
          $(e).addClass('ignored');
        }
        return $(e).show();
      });
    };

    StoryListView.prototype.undoLastIgnore = function() {
      var lastEntryId;
      lastEntryId = this.manager.last();
      if (lastEntryId != null) {
        this.manager.removeIgnore(lastEntryId);
        $("#" + lastEntryId).removeClass("ignored").show().find(".fanficplus-ignore").html("<img class='fanficplus-del' src='" + icons.del + "' alt='Ignore' />");
        return true;
      } else {
        return false;
      }
    };

    StoryListView.prototype.clearIgnores = function() {
      var _this = this;
      this.manager.truncateIgnores();
      this.$entryEl().each(function(i, e) {
        var target;
        target = $(e).find(".fanficplus-ignore");
        target.html("<img class='fanficplus-del' src='" + icons.del + "' alt='Ignore' />");
        return target.parent().removeClass("ignored").show();
      });
      return true;
    };

    StoryListView.prototype.ignoreAll = function() {
      var _this = this;
      return this.$entryEl().each(function(i, e) {
        var target;
        _this.manager.addIgnore(e.id);
        target = $(e).find(".fanficplus-ignore");
        target.html("<img class='fanficplus-add' src='" + icons.add + "' alt='Unignore' />");
        return target.parent().hide();
      });
    };

    StoryListView.prototype.insertHeaderLinks = function() {
      var controls, target,
        _this = this;
      this.$controlsEl().prepend("<div id='fanficplus-controls' class='clearfix'></div>");
      target = $("#fanficplus-controls");
      controls = {
        clear_all: {
          klass: 'add',
          src: icons.add,
          text: 'Clear All Ignores'
        },
        ignore_all: {
          klass: 'del',
          src: icons.del,
          text: 'Ignore all'
        },
        undo_last: {
          klass: 'undo',
          src: icons.undo,
          text: 'Undo last ignore'
        },
        reveal_hidden: {
          klass: 'show',
          src: icons.show,
          text: 'Reveal Hidden'
        }
      };
      controls = _.map(controls, function(v, k) {
        return "<a href='#' class='fanficplus-" + k + "'>       <img class='fanficplus-" + v.klass + "' src='" + v.src + "' alt='" + v.text + "' />       " + v.text + "       </a>";
      });
      target.append(controls.join("&nbsp;|&nbsp;"));
      return this.addHeaderEvents();
    };

    StoryListView.prototype.addHeaderEvents = function() {
      var _this = this;
      this.$ignoreEl().click(function(e) {
        var id;
        id = $(e.currentTarget).attr('data-storyid');
        if (_this.manager.hasIgnore(id)) {
          _this.manager.removeIgnore(id);
          $(e.currentTarget).html("<img class='fanficplus-del' src='" + icons.del + "' alt='Ignore' />");
          $(e.currentTarget).parent().toggleClass('ignored').show();
        } else {
          _this.manager.addIgnore(id);
          $(e.currentTarget).html("<img class='fanficplus-add' src='" + icons.add + "' alt='Unignore' />");
          $(e.currentTarget).parent().hide();
        }
        return false;
      });
      this.$clearAllEl().click(function() {
        if (confirm("Are you sure you want to clear all ignores? This is irreversible!")) {
          _this.clearIgnores();
        }
        return false;
      });
      this.$ignoreAllEl().click(function() {
        if (confirm("Do you really want to ignore all fanfic on this page? This is reversible, but it's a pretty tedious process.")) {
          _this.ignoreAll();
        }
        return false;
      });
      this.$undoLastEl().click(function() {
        _this.undoLastIgnore();
        return false;
      });
      return this.$revealHiddenEl().click(function() {
        _this.revealHidden();
        return false;
      });
    };

    return StoryListView;

  })();

  $(function() {
    var view;
    view = new StoryListView();
    view.$entryEl().find('a:first-child').each(function(i, e) {
      var entry, id;
      id = e.href.split("/")[4];
      entry = $(e).parent();
      entry.attr("id", id);
      if (view.manager.hasIgnore(id)) {
        entry.prepend("<a href='#' class='fanficplus-ignore' data-storyid='" + id + "'><img class='fanficplus-add' src='" + icons.add + "' alt='Unignore' /></a>&nbsp;");
        return entry.hide();
      } else {
        return entry.prepend("<a href='#' class='fanficplus-ignore' data-storyid='" + id + "'><img class='fanficplus-del' src='" + icons.del + "' alt='Ignore' /></a>&nbsp;");
      }
    });
    return view.insertHeaderLinks();
  });

}).call(this);
